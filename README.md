#Assignment 1

Nii Mante

CSCI 544 - Applied Natural Language Processing 

##Purpose

This assignment is for learning how to create our own text classifier. We'll web be taking on two tasks.  (1) Spam filtering and (2) Sentiment Analysis. 

##Relevant information

- I converted files to a corpus/concatenated text file with my `preprocess.py` script.
- I generated f scores on dev data with my `get_f_scores.py` script.
- I used add one smoothing, an unknown keyword, and a number keyword for my model/classifier. 

##Precision, Recall and F-Score - Part 1

###SPAM
===

- SPAM overall F-Score = 0.9772560528246516
- SPAM individual Precision: 0.9486486486486486
- SPAM individual Recall: 0.9669421487603306
- SPAM individual f score: 0.9577080491132333
- HAM individual Precision: 0.9879154078549849
- HAM individual Recall: 0.981
- HAM individual f score: 0.9844455594581033

###Sentiment
===

- SENTIMENT F-Score = 0.83125
- POS individual Precision: 0.8590785907859079
- POS individual Recall: 0.7925
- POS individual f score: 0.8244473342002601
- NEG individual Precision: 0.8074245939675174
- NEG individual Recall: 0.87
- NEG individual f score: 0.8375451263537906

##Precision, Recall and F-Score - Part 2

###SVM

- Dev data error rates
	- sentiment megam - 759 / 1600 = 0.474375
	- spam megam - 362 / 1363 = 0.265591
- Test data Error Rates
	- spam megam - 41 / 2725 = 0.0150459
	- sentiment megam - 9108 / 25000 = 0.36432

***Spam f-scores***

- SPAM F-Score = 0.7219369038884813
- HAM individual Precision: 0.7343396226415094
- HAM individual Recall: 0.973
- HAM individual f score: 0.8369892473118279
- SPAM individual Precision: 0.2894736842105263
- SPAM individual Recall: 0.030303030303030304
- SPAM individual f score: 0.05486284289276809

***Sentiment f-scores***

- SENTIMENT F-Score = 0.518125
- POS individual Precision: 0.5736040609137056
- POS individual Recall: 0.14125
- POS individual f score: 0.22668004012036105
- NEG individual Precision: 0.5103349964362082
- NEG individual Recall: 0.895
- NEG individual f score: 0.6500226963231956

###MEGAM

***Sentiment f-scores***

- SENTIMENT F-Score = 0.525625
- NEG individual Precision: 0.5170975813177648
- NEG individual Recall: 0.775
- NEG individual f score: 0.6203101550775388
- POS individual Precision: 0.5511221945137157
- POS individual Recall: 0.27625
- POS individual f score: 0.3680266444629475


***Spam f-scores***
- SPAM F-Score = 0.7344093910491563
- SPAM individual Precision: 1.0
- SPAM individual Recall: 0.0027548209366391185
- SPAM individual f score: 0.005494505494505495
- HAM individual Precision: 0.7342143906020558
- HAM individual Recall: 1.0
- HAM individual f score: 0.8467400508044032

##Further Questions

- What happens to precision, recall and F-Score in each of the two tasks (on the development data) when only 10% of the training data is used to train the classifiers in part 1 and part 2? 

###SPAM scores when training size reduced with part 1

- SPAM F-Score = 0.9677182685253118
- HAM individual Precision: 0.9704724409448819
- HAM individual Recall: 0.986
- HAM individual f score: 0.9781746031746031
- SPAM individual Precision: 0.9596541786743515
- SPAM individual Recall: 0.9173553719008265
- SPAM individual f score: 0.9380281690140845

###SENTIMENT scores when training size reduced with part 1
- SENTIMENT F-Score = 0.81875
- POS individual Precision: 0.839095744680851
- POS individual Recall: 0.78875
- POS individual f score: 0.8131443298969072
- NEG individual Precision: 0.8007075471698113
- NEG individual Recall: 0.84875
- NEG individual f score: 0.8240291262135923


###SPAM scores when training size reduced with part 2, svm_light

###SPAM scores when training size reduced with part 2, megam

###SENTIMENT scores when training size reduced with part 2, svm_light

###SENTIMENT scores when training size reduced with part 2, megam



- Why do you think that is?

I believe that the training set still has enough features to distinguish between spam and ham as well as pos and neg.  Ultimately, if the model contains enough parameters to estimate a class, then precision and recall won't suffer too much.  I think that if we were to continue shrinking the training set eventually f-scores would drop off rapidly.  This would be due to the fact that there would not be enough features to represent the many different documents that would be presented to our classifier.  



##Appendix

###Svm_learn output

- Spam

		(5233 iterations)
		Optimization finished (201 misclassified, maxdiff=0.00098).
		Runtime in cpu-seconds: 7.46
		Number of SV: 3572 (including 2912 at upper bound)
		L1 loss: loss=1144.90663
		Norm of weight vector: |w|=2908.21915
		Norm of longest example vector: |x|=2.06162
		Estimated VCdim of classifier: VCdim<=26624875.68336
		Computing XiAlpha-estimates...done
		Runtime for XiAlpha-estimates in cpu-seconds: 0.00
		XiAlpha-estimate of the error: error<=19.35% (rho=1.00,depth=0)
		XiAlpha-estimate of the recall: recall=>63.82% (rho=1.00,depth=0)
		XiAlpha-estimate of the precision: precision=>63.59% (rho=1.00,depth=0)
		Number of kernel evaluations: 483046
		Writing model file...done
		
- Sentiment

		(5979 iterations)
		Optimization finished (2188 misclassified, maxdiff=0.00097).
		Runtime in cpu-seconds: 21.10
		Number of SV: 12572 (including 10737 at upper bound)
		L1 loss: loss=6853.66800
		Norm of weight vector: |w|=72.55776
		Norm of longest example vector: |x|=132.98840
		Estimated VCdim of classifier: VCdim<=93109762.57862
		Computing XiAlpha-estimates...done
		Runtime for XiAlpha-estimates in cpu-seconds: 0.01
		XiAlpha-estimate of the error: error<=53.73% (rho=1.00,depth=0)
		XiAlpha-estimate of the recall: recall=>46.05% (rho=1.00,depth=0)
		XiAlpha-estimate of the precision: precision=>46.26% (rho=1.00,depth=0)
		Number of kernel evaluations: 600506