# Convert nblearn training and text files to svm then megam format

echo "-----------------------"
echo "-- Converting to svm --"
echo "-----------------------"
echo "-------   SPAM  -------"
echo "-----------------------"

echo "------ Converting spam_training.txt to spam_svm_training.txt"
#python3 convert_file_format.py -t -i spam_training.txt --svm svm_spam_training.txt

echo "------ Converting spam_dev.txt to spam_svm_dev.txt"
#python3 preprocess.py -d SPAM_dev spam-dev.txt
#python3 convert_file_format.py -t -i spam-dev.txt --svm svm_spam_dev.txt

echo "------ Converting spam_testfile.txt to spam_svm_testing.txt"
python3 convert_file_format.py -i spam_testfile.txt --svm svm_spam_testing.txt

echo "-----------------------"
echo "------ SENTIMENT ------"
echo "-----------------------"
echo "------ Converting sentiment_training.txt to sentiment_svm_training.txt"
#python3 convert_file_format.py -t -i sentiment_training.txt --svm svm_sentiment_training.txt

echo "------ Converting sentiment_dev.txt to sentiment_svm_dev.txt"
#python3 preprocess.py -d SENTIMENT_dev sentiment-dev.txt
#python3 convert_file_format.py -t -i sentiment-dev.txt --svm svm_sentiment_dev.txt

echo "------ Converting sentiment_testfile.txt to spam_sentiment_testing.txt"
python3 convert_file_format.py -i sentiment_testfile.txt --svm svm_sentiment_testing.txt


#echo "-----------------------"
#echo "-- Convert to megam  --"
#echo "-----------------------"
#echo "-------   SPAM  -------"
#echo "-----------------------"
#
#echo "------ Replacing -1 class labels with 0 for megam"
#echo "------ Also replacing ':' with ' ' for megam"
#
#cp svm_spam_training.txt megam_spam_training.txt
#sed -i '' 's/-1/0/g' megam_spam_training.txt
#sed -i '' 's/:/\ /g' megam_spam_training.txt
#
#cp svm_spam_dev.txt megam_spam_dev.txt
#sed -i '' 's/-1/0/g' megam_spam_dev.txt
#sed -i '' 's/:/\ /g' megam_spam_dev.txt
#
#cp svm_spam_testing.txt megam_spam_testing.txt
#sed -i '' 's/-1/0/g' megam_spam_testing.txt
#sed -i '' 's/:/\ /g' megam_spam_testing.txt
#
#echo "-----------------------"
#echo "------ SENTIMENT ------"
#echo "-----------------------"
#
#echo "------ Replacing -1 class labels with 0 for megam"
#echo "------ Also replacing ':' with ' ' for megam"
#
#cp svm_sentiment_training.txt megam_sentiment_training.txt
#sed -i '' 's/-1/0/g' megam_sentiment_training.txt
#sed -i '' 's/:/\ /g' megam_sentiment_training.txt
#
#cp svm_sentiment_dev.txt megam_sentiment_dev.txt
#sed -i '' 's/-1/0/g' megam_sentiment_dev.txt
#sed -i '' 's/:/\ /g' megam_sentiment_dev.txt 
#
#cp svm_sentiment_testing.txt megam_sentiment_testing.txt
#sed -i '' 's/-1/0/g' megam_sentiment_testing.txt
#sed -i '' 's/:/\ /g' megam_sentiment_testing.txt
#

echo "-----------------------"
echo "---- Moving Files -----"
echo "-----------------------"

#mv megam_* part2/
mv svm_* part2/

#echo "------ Change into part2 directory"
#cd part2/
#echo "------ Concatenate the megam spam dev and training files together"
#cat megam_spam_training.txt > megam.spam.intermediate
#echo "DEV" >> megam.spam.intermediate
#cat megam.spam.intermediate megam_spam_dev.txt > megam.spam.training.txt
#echo "------ Removing individual megam spam dev and training files"
#rm megam_spam_dev.txt megam_spam_training.txt megam.spam.intermediate
#
#echo "------ Concatenate the megam sentiment and dev training files together"
#cat megam_sentiment_training.txt > megam.sentiment.intermediate
#echo "DEV" >> megam.sentiment.intermediate
#cat megam.sentiment.intermediate megam_sentiment_dev.txt > megam.sentiment.training.txt
#echo "------ Removing individual megam sentiment dev and training files"
#rm megam_sentiment_dev.txt megam_sentiment_training.txt megam.sentiment.intermediate 
#

echo "---------------------------------------"
echo "---------------- DONE -----------------"
echo "---------------------------------------"
