# Execute all steps from preprocessing, to learning to classifying

echo "------ Removing spam and sentiment training/testing/model files"
rm spam_dev.txt spam_training.txt sentiment_dev.txt sentiment_training.txt 
rm dev_spam.out spam.nb dev_sentiment.out sentiment.nb 

echo "------ Preprocessing training and dev files"
python3 preprocess.py -d SPAM_training spam_training.txt
python3 preprocess.py -t -d SPAM_dev spam_dev.txt
python3 preprocess.py -d SENTIMENT_training sentiment_training.txt
python3 preprocess.py -t -d SENTIMENT_dev sentiment_dev.txt

echo "------ Spam Learning"
python3 nblearn.py spam_training.txt spam.nb
echo "------ Sentiment Learning"
python3 nblearn.py sentiment_training.txt sentiment.nb

echo "------ Spam Classification" 
python3 nbclassify.py spam.nb spam_dev.txt > dev_spam.out
echo "Spam classification done. Results in dev_spam.out"
echo "------ Sentiment Classification" 
python3 nbclassify.py sentiment.nb sentiment_dev.txt > dev_sentiment.out
echo "Sentiment classification done. Results in dev_sentiment.out"

echo "------ Spam F-Score"
python3 get_f_scores.py SPAM_dev dev_spam.out

echo "------ Sentiment F-Score"
python3 get_f_scores.py SENTIMENT_dev dev_sentiment.out

#echo "------ PART 2"
#echo "------ Generate SPAM svm training file"
#python3 convert_file_format.py -t -i spam_training.txt --svm part2/spam_svm_training.txt


