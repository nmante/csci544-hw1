# Execute all steps from preprocessing, to learning to classifying

echo "------ Removing spam and sentiment training/testing/model files"
rm spam_testfile.txt spam_training.txt sentiment_testfile.txt sentiment_training.txt 
rm spam.out spam.nb sentiment.out sentiment.nb 

echo "------ Preprocessing training and dev files"
python3 preprocess.py -d SPAM_training spam_training.txt
python3 preprocess.py -t -d SPAM_test spam_testfile.txt
python3 preprocess.py -d SENTIMENT_training sentiment_training.txt
python3 preprocess.py -t -d SENTIMENT_test sentiment_testfile.txt

echo "------ Spam Learning"
python3 nblearn.py spam_training.txt spam.nb
echo "------ Sentiment Learning"
python3 nblearn.py sentiment_training.txt sentiment.nb

echo "------ Spam Classification" 
python3 nbclassify.py spam.nb spam_testfile.txt > spam.out
echo "Spam classification done. Results in spam.out"
echo "------ Sentiment Classification" 
python3 nbclassify.py sentiment.nb sentiment_testfile.txt > sentiment.out
echo "Sentiment classification done. Results in sentiment.out"

echo "----- Cleaning up test files"
rm sentiment_testfile.txt spam_testfile.txt 

#echo "------ Spam F-Score"
#python3 get_f_scores.py SPAM_dev spam.out

#echo "------ Sentiment F-Score"
#python3 get_f_scores.py SENTIMENT_dev sentiment.out
