"""
    The purpose of this script is to convert my test files to 
    to the svm_light test file format.

        <LABEL> <feature> ... <feature>

    It needs to be converted to this:

        <target> <feature>:<value> ... <feature_N>:<value_N> # comment

"""
import argparse
import os
import sys
import math

def dictKey(sortedTupleList):
    return sortedTupleList[1]['featureValue']

def create_parser():
    parser = argparse.ArgumentParser(
            description = 'Convert my nblearn model file to a different test format')
    parser.add_argument(
            '-i',
            '--input',
            type=argparse.FileType('r'),
            help='The file to convert to a new format')
    parser.add_argument(
            '-t',
            '--training',
            action='store_true',
            help='If the input file is from test data pass this option')
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
            '--svm',
            action='store_true',
            help='Convert this file to svm format')
    group.add_argument(
            '--megam',
            action='store_true',
            help='Convert this file to megam format')
    parser.add_argument(
            'outputFile',
            type=argparse.FileType('w'),
            help='The output file containing the conversion')


    return parser

def tf_idf(testFile, args, labelDict):
    """
        For each document...
            calculate the number of times a word appears
            calculate how many words are in that document
        after done with doc increment how many num documents with this word in it
        increment number of documents
    """
    termDict = {} 
    
    docs = []
    docIndex = 0
    documentInfo = {}
    wordInDocDict = {}
    isTraining = args.training
    wordListDict = {} 
    featureValue = 1 
    labelList = []
    #labelDict = { 0 : -1, 1 : 1 } 
    for document in testFile:
        numWords = 0
        termDict = {}
        termDict['words'] = {}
        termDict['label'] = ""
        for word in document.split():
            if isTraining and numWords == 0:
                #i = labelList.index(word) if word in labelList else None
                #if i is None:
                if word not in labelList:
                    labelList.append(word)
                labelIndex = labelList.index(word)
                termDict['label'] = labelDict[labelIndex]
            else:    
                #i = wordList.index(word) if word in wordList else None
                #if i is None: 
                if wordListDict.get(word) is None:
                    wordListDict[word] = featureValue
                    featureValue += 1

                if wordInDocDict.get(word) is None:
                    wordInDocDict[word] = set()

                if termDict['words'].get(word) is None:
                    termDict['words'][word] = {}
                    termDict['words'][word]['count'] = 0
                    termDict['words'][word]['tf.idf'] = 0
                termDict['words'][word]['count'] += 1
                termDict['words'][word]['featureValue'] = wordListDict[word]
                wordInDocDict[word].add(docIndex)

            numWords += 1
        sortedTermList = sorted(list(termDict['words'].items()), key=dictKey)
        termDict['sortedTermList'] = sortedTermList
        docs.append(termDict)
        docIndex += 1

    totalNumDocuments= len(docs) 
    
           
    documentInfo['tf'] = docs
    documentInfo['wordListDict'] = wordListDict
    documentInfo['wordsInDoc'] = wordInDocDict

    #docs = tf_idf(inputFile, args, {0 : -1, 1 : 1})
    totalNumDocuments = len(documentInfo['tf'])
    #testFile.seek(0,0)
    docIndex = 0
    
    for termDict in docs:
        lineToWrite = ''
        numWords = 0
        for wordTuple in termDict['sortedTermList']:
            if isTraining and numWords == 0:
                lineToWrite += str(termDict['label']) + ' '
                numWords += 1
                continue
            elif numWords == 0:
                lineToWrite += '0 '
                numWords += 1
                continue
            #featureNumber = termDict['words'][word]['featureValue'] 
            featureNumber = wordTuple[1]['featureValue']
            wordsInDoc = termDict['words']
            #wordDict = wordsInDoc[word]
            wordDict = wordTuple[1]
            wordCount = wordDict['count']
            numWordInDoc = len(wordsInDoc.keys())
            numDocsWordIn = len(list(documentInfo['wordsInDoc'][word])) 
            tf = wordCount/numWordInDoc
            idf = math.log(totalNumDocuments/numDocsWordIn)
            value = tf*idf
            if value != 0.0:
                strValue = "%.18f" % value 
                lineToWrite += str(featureNumber) + ':' + strValue + ' '
            numWords += 1
        args.outputFile.write(lineToWrite + '\n')
        docIndex += 1


            
    #return documentInfo 

def convert_files(args):
    doSvm = args.svm
    doMegam = args.megam
    inputFile = args.input
    isTraining = args.training
    if doSvm:
        tf_idf(inputFile, args, {0 : -1, 1 : 1})
    elif doMegam:
        tf_idf(inputFile, args, {0 : 0, 1 : 1})
    # Take in a model file
    # Take in an output testing file
    # for every word in the input file on a line, replace the word with a
    # feature:value pair
    # The value comes from the model file
    # and should be chosen based on the label

    # We'll compute the tf.idf for each word and that will be the feature
    
def main():
    # create parser
    parser = create_parser()
    args = parser.parse_args()

    convert_files(args)

if __name__ == "__main__":
    main()
        
