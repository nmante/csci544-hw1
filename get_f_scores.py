"""
    The purpose of this program is to calculate the F scores
    for our classifier

    We'll have multiple types of input training, test and output files,
    so this will be as general as possible so that we can reuse it

    This program is executed as follows:

        python3 get_f_scores.py TESTDIRECTORY LABELFILE 

    where TESTDIRECTORY is the directory that contains all of test 
    documents prior to processing e.g.
        
        SPAM_dev/
            SPAM.00011.txt
            .
            .
            .

    and LABELFILE is where we outputted our answers/classifications to

"""
import argparse
import os
import pdb

def create_parser():
    # Create a parser that takes two mandatory arguments
    parser = argparse.ArgumentParser(
            description='This program calculates F scores of a classifier')
    parser.add_argument(
            'testDirectory',
            help='The directory that contains all of our test files')
    parser.add_argument(
            'labelFile',
            type=argparse.FileType('r'),
            help='The output file from nbclassify. It contains one classification per line')

    return parser

def generate_f_scores(args):
    # The goal is to calculate the precision and recall of a class C 
    testDirectory = args.testDirectory
    labelFile = args.labelFile

    # Get each classification aka line, and put it into an array
    classifications = [line.rsplit('\n')[0] for line in labelFile]
    classes = {}

    for root, dirs, files in os.walk(testDirectory):
        ind = 0
        for f in files:
            # The label is the first part of the file name preceeding the '.' 
            # character.  Let's just split the string and grab it
            actual = f.split('.')[0]

            # If we haven't encountered this label (i.e. SPAM or HAM or whatever)
            # then add it into our classes object
            if actual not in classes:
                classes[actual] = {}
                classes[actual]["belongs"] = 0
                classes[actual]["classified_as"] = 0
                classes[actual]["correct"] = 0
            
            # If our output text yielded the right classification
            # increment the 'correct' score for this class
            if classifications[ind] == actual:
                classes[actual]["correct"] += 1

            classes[actual]["belongs"] += 1
            predicted = classifications[ind]
            if predicted not in classes:
                classes[predicted] = {}
                classes[predicted]["belongs"] = 0
                classes[predicted]["classified_as"] = 0
                classes[predicted]["correct"] = 0

            classes[predicted]["classified_as"] +=1
            ind += 1
    
    print(classes)
    # Total Precision
    precisionNumerator = sum([classes[label]["correct"] for label in classes]) 
    precisionDenominator = sum([classes[label]["classified_as"] for label in classes])
    precision = precisionNumerator/precisionDenominator 
    
        
    # Total Recall
    recallNumerator = sum([classes[label]["correct"] for label in classes])
    recallDenominator = sum([classes[label]["belongs"] for label in classes])
    recall = recallNumerator/recallDenominator

    
    fScore = 2 * precision * recall / (precision + recall)
    cName = testDirectory.split('_')[0]
    print(cName + ' F-Score = ' + str(fScore))
   
    for label in classes:

    #if cName == 'SPAM':
        # Precision for SPAM
        spamPrecisionNumerator = classes[label]["correct"]
        spamPrecisionDenominator = classes[label]["classified_as"]
        spamPrecision = spamPrecisionNumerator/spamPrecisionDenominator

        # Recall for SPAM
        spamRecallNumerator = classes[label]["correct"]
        spamRecallDenominator = classes[label]["belongs"]
        spamRecall = spamRecallNumerator/spamRecallDenominator
        spamFScore = 2 *spamPrecision * spamRecall / (spamPrecision + spamRecall)
        print(label + " individual Precision: " + str(spamPrecision))
        print(label + " individual Recall: " + str(spamRecall))
        print(label + " individual f score: " + str(spamFScore))

# Execute this if we are the main module
def main():
    # Create a parser
    parser = create_parser()
    args = parser.parse_args()

    # Generate the scores
    generate_f_scores(args)

if __name__ == "__main__":
    main()
