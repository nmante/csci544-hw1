#!/usr/bin/python
"""
    Preprocessing file
    Nii Mante
    CSCI 544
    
    This script concatenates training data files together into 
    one file
"""

import argparse
import os
import sys

# Parse command line arguments
def create_parser():
    parser = argparse.ArgumentParser(
            description='This program concatenates training file into one large feature file with labels')

    parser.add_argument(
            '-t',
            '--testing',
            dest='isTesting',
            action='store_true',
            default=False,
            help='Output a TESTINGFILE instead of a TRAININGFILE')

    parser.add_argument(
            '-d',
            '--directory',
            dest='dirName',
            required=True, 
            help='The directory which contains the training files') 

    parser.add_argument(
            'outputFile',
            type=argparse.FileType('w'),  
            help='The resulting TRAINING_FILE') 

    return parser

# Convert a file into a feature string
def process_file(pathName, isTesting):
    if not os.path.isfile(pathName):
        print("Error: While extracting feature string, encountered an invalid file")
        sys.exit()

    # Get ONLY the file name and split the name via '.'
    # The first element will be the label
    fileName = os.path.basename(pathName) 
    strs = fileName.split('.')
    label = ''
    if not isTesting:
        label = strs[0]

    # Open the file, and add a empty space, and each line of 
    # the message to a string 
    f = open(pathName, 'r', errors='ignore')
    for line in f:
        label = label + ' ' + line.rstrip('\n')

    return label + '\n' 

# Look at all files in directory, and write feature strings 
# to the desired output file

def process_directory(dirName, outputFile, isTesting):
    if not os.path.exists(dirName): 
        print('Error: Directory does not exist')
        sys.exit()
    # For all files in directory, 
    for root, dirs, files in os.walk(dirName):
        for f in files:
            if f.endswith('.txt'):
                # Process the file into a single string
                fullFileName = os.path.join(dirName, f)
                featureString = process_file(fullFileName, isTesting)
                # Write the string to our desired output file 
                outputFile.write(featureString)

    outputFile.close()
        
def main():
    # Parse Command Line arguments
    parser = create_parser()
    args = parser.parse_args()

    # Process a directory and output a file based on
    # the command line arguments
    dirName = args.dirName
    outputFile = args.outputFile
    isTesting = args.isTesting
    process_directory(dirName, outputFile, isTesting)


if __name__ == "__main__":
    main()
