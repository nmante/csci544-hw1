"""
    This script takes in a <modelFile> and a <testFile>, and classifies
    the test file based on the model

    For example, if we have two classes SPAM and HAM (different types of email)
    then this classifier would use a model generated for spam/ham to 'guess'
    what our <testFile>  

    This file is executed as follows:

        python3 nbclassify.py MODELFILE TESTFILE

    where MODELFILE is a previously trained file outputted from nblearn.py, and
    TESTFILE is an unlabeled input file we want to classify

    The TESTFILE has a format like so:

        feature11 feature12 ... feature1N
        feature21 feature22 ... feature2N
        .
        .
        .
        featureM1 featureM2 ... featureMN

"""
import argparse
import json
import math

def create_parser():
    # Create the parser. Let it take a modelfile and testfile
    parser = argparse.ArgumentParser(
            description='This program classifies a document <TESTFILE> based on a <MODELFILE>')

    parser.add_argument(
            'modelFile',
            type=argparse.FileType('r'),
            help='A modelfile which contains a model for our classes of interest') 
    parser.add_argument(
            'testFile',
            type=argparse.FileType('r'),
            help='A test file which contains document(s) represented as rows of features.') 

    return parser

def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def getFeaturizedWord(word):
    if is_number(word):
        return "<num>"
    else:
        return word

    
def generate_classification(modelFile, testFile):
    """
        Load in our model
        It has a format like so:

        classes = {
            
            <label_name> : {
                "count" : <this is the number of documents with this label> 
                "num_features> : <number of total words in this class>,
                "words" : {
                    <word_1> : <number of times this word appears>,
                    <word_2> : <count>,
                    <word_n> : <count>
                }
            }
        }
    """
    classes = json.load(modelFile)

    # Now we will classify using Bayes Theorem, and more spefically, naive bayes
    # For each document in the test file
    totalNumLabels = sum([val['count'] for (key, val) in classes.items()])
    i = 0 
    for document in testFile:
        if i > 0:
            break
        #maxProb = (0, '') 
        maxProb = []
        for class_ in classes.keys():
            #print(class_)
            # numFeaturesInClass is total number of nonunique words
            numFeaturesInClass = classes[class_]['numFeatures']

            # numLabels in class is total number of HAM or SPAM in training set
            numLabelsInClass = classes[class_]['count']

            # vocabSize is the total amount unique words in all classes.
            # Thus, the vocab size should be the same for each class
            vocabSize = classes[class_]['vocabSize']

            # pClass is the maximum likelyhood probability of a class occuring
            pClass = math.log(numLabelsInClass/totalNumLabels) 
            
            # Dictionary/Hash containing our words and conditional probabilities (as values)
            wordDict = classes[class_]['words']
            pUnknownWord = math.log(1/(numFeaturesInClass + vocabSize + 1)) 
            # pDocumentClass is probability of a document given a class
            pDocumentClass = sum([wordDict.get(getFeaturizedWord(word),pUnknownWord) for word in document.split()])

            # pClassDocument is the result we want
            pClassDocument = (pClass + pDocumentClass)
            maxProb.append((pClassDocument, class_))

        # We've finished processing one document, so print our guess for the doc
        #i += 1
        #print(maxProb)
        print(max(maxProb)[1])


def main():
    # Parse the command line arguments
    parser = create_parser()
    args = parser.parse_args()
    
    # Read in the model file
    modelFile = args.modelFile
    testFile = args.testFile

    # Generate the classification for each document and
    # output each classification as one line to stdout
    generate_classification(modelFile, testFile)

if __name__ == "__main__":
    main()
