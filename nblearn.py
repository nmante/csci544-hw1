"""
    nblearn.py

    This file is responsible for learning a model based on
    a singular input training file.
    The training file is of the form

        LABEL_1 FEATURE_11 FEATURE_12 ... FEATURE_1N
        LABEL_2 FEATURE_21 FEATURE_22 ... FEATURE_2N
        .
        .
        .
        LABEL_N FEATURE_M1 FEATURE_M2 ... FEATURE_MN

    Once it's trained on the data, it should output a model file.
    
    This program is executed like so:

        python3 nblearn.py TRAININGFILE MODELFILE
"""
import argparse
import sys
import os
import json
import math
import operator

def create_parser():
    parser = argparse.ArgumentParser(description='This program generates a naive bayes model by training on input data')
    parser.add_argument(
            'trainingFile',
            type=argparse.FileType('r'),
            help="""
                A single input file of the form

                    LABEL_1 FEATURE_11 FEATURE_12 ... FEATURE_1N
                    LABEL_2 FEATURE_21 FEATURE_22 ... FEATURE_2N
                    .
                    .
                    .
                    LABEL_N FEATURE_M1 FEATURE_M2 ... FEATURE_MN
            """)
    parser.add_argument(
            'modelFile',
            type=argparse.FileType('w'),
            help='The output model file.')

    return parser
def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def getFeaturizedWord(word):
    if is_number(word):
        return "<num>"
    else:
        return word

def generate_model(trainingFile, modelFile):
    tFile = trainingFile

    # Model V1 is a bag of words model
    # For each unique word, we store a count
    # Let's store everything in a dictionary
    # whenever we encounter a word, we just increment the value
    # if the word isn't there, we just create an key-value pair with the 
    # initial value as 1

    classes = {} 
    #classes["total_count"] = 1
    vocabSet = set()

    for line in tFile:
        tokenizedLine = line.split()
        label = tokenizedLine[0]
        if label not in classes:
            classes[label] = {}
            classes[label]["numFeatures"] = 0
            classes[label]["count"] = 1
            classes[label]["words"] = {}
        else:
            classes[label]["count"] += 1 

        # for each word in the line, add it to it's class and increment
        # the count. Each line has a LABEL, so we should ignore it
        # So just start from index 1 to do that
        for i in range(1, len(tokenizedLine)): 
            word = tokenizedLine[i]
            word = getFeaturizedWord(word)
            # Add this word to our set. It will take care of adding unique elements
            vocabSet.add(word)
            # If we've never seen this word, add it to the words dict
            if word not in classes[label]["words"]: 
                classes[label]["words"][word] = 1

            # Increase how many words/numFeatures we've seen, and how many times a particular
            # word has been seen
            classes[label]["numFeatures"] += 1
            classes[label]["words"][word] += 1  

    for label in classes:
        classes[label]['vocabSize'] = len(list(vocabSet))

    for label in classes:
        wordDict = classes[label]["words"]
        numFeat = classes[label]["numFeatures"]
        vocabSize = classes[label]["vocabSize"]
        for word in wordDict:
            wordCount = wordDict[word]
            wordDict[word] = math.log(wordCount/(numFeat + vocabSize))

    json.dump(classes, modelFile)

def main():

    # Grab the command line arguments and parse them
    parser = create_parser()
    args = parser.parse_args() 

    # Read in the training file and generate the model
    trainingFile = args.trainingFile
    modelFile = args.modelFile
    generate_model(trainingFile, modelFile)

# Only invoke our main() function if this module is being
# executed as the main file
if __name__ == "__main__":
    main()
