"""
    The purpose of this script is to convert the output from
    svm_light or megam to our desired output
"""
import argparse

def create_parser():
    parser = argparse.ArgumentParser(
            description='Convert output from svm_light or megam to string labels')
    parser.add_argument(
            '-i',
            '--input',
            type=argparse.FileType('r'),
            help='The file to convert')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
            '--svm',
            action='store_true')
    group.add_argument(
            '--megam',
            action='store_true')
    group2 = parser.add_mutually_exclusive_group(required=True)
    group2.add_argument(
            '--spam',
            action='store_true')
    group2.add_argument(
            '--sentiment',
            action='store_true')
    parser.add_argument(
            'outputFile',
            type=argparse.FileType('w'),
            help='The output file with converted labels')

    return parser

def convert_file(args):
    # Add conversion code
    isSvm = args.svm
    isMegam = args.megam
    isSpam = args.spam
    isSentiment = args.sentiment

    inputFile = args.input
    outputFile = args.outputFile
    label = {}
    if isSvm:
        label = { '-1' : 'HAM', '1' : 'SPAM'} 
        if isSpam:
            label = { '-1' : 'HAM', '1' : 'SPAM'} 
        elif isSentiment:
            label = { '-1' : 'NEG', '1' : 'POS'} 
        for line in inputFile:
            output = line.split()
            if float(output[0]) <= 0:
                outputFile.write(label['-1'] + '\n')
            elif float(output[0]) > 0:
                outputFile.write(label['1'] + '\n')

    elif isMegam:
        if isSpam:
            label = { '0' : 'HAM', '1' : 'SPAM'}
        elif isSentiment:
            label = { '0' : 'NEG', '1' : 'POS'}

        for line in inputFile:
            output = line.split()
            outputFile.write(label[output[0]] + '\n')


def main():
    parser = create_parser()
    args = parser.parse_args()

    convert_file(args)

if __name__ == "__main__":
    main()

