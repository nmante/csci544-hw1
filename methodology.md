#Homework 1 Methodology

##Purpose

- Our goal for the assignment is to build a text classifier
- The classifier is going to be a Naive Bayes Classifier
- We are classifying spam or ham messages; those are the two classes
- As well as sentiment analysis

## Part 1

- We'll be writing two python files
- One python file will be responsible for learning the model based on the Training data
	- nblearn.py
- Another file will be responsible for classifiying NEW text
	- nbclassify.py
	
### Learning from data

- We'll be doing 
	- Spam classification and
	- Sentiment Analysis
- In spam classification, one document is one email message
- Classification *labels* for the spam classifier are
	- SPAM
	- HAM
- In sentiment analysis, one document is one movie review

####Tasks

- **First Task** - Format the training data
	- Take the SPAM/HAM files and translate them to one training file
	- The classifier must accept training data in ONE file with the following format
	
			LABEL_1 FEATURE_11 FEATURE_12 ... FEATURE_1N
			LABEL_2 FEATURE_21 FEATURE_22 ... FEATURE_2N
			.
			.
			.
			LABEL_M FEATURE_M1 FEATURE_M2 ... FEATURE_MN
	
- Once you have this *training file*, your goal is to create a *model file* from the training data!
- The *model file* should have all relevant data necessary to classify test data
- Additionally, you should be able to classify test data using the model file without having to repeatedely train

- **IMPORTANT:** Your code to learn a model should be invoked like so
	
		python3 nblearn.py TRAININGFILE MODELFILE
		# where TRAININGFILE is one input file with all training data
		# and MODELFILE is an output file containing necessary info for classification

- For SPAM analysis, the TRAININGFILE should be named `spam_training.txt`
- For Sentiment analysis, the TRAININGFILE should be `sentiment_training.txt`
- The MODELFILE should be `spam.nb` or `sentiment.nb`

### Classifying new text

- **Second Task**







